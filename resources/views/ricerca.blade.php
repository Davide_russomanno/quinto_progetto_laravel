<x-layout>

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h1>Risultati della tua ricerca: {{ $key }}</h1>
            </div>
        </div>
        <div class="row my-3">
            @if(count($games) > 0)
                @foreach($games as $game)
                    <div class="col-12 col-md-3 my-5">
                        <div class="card" style="width: 18rem;">
                            <img src="{{ $game['img'] }}" class="card-img-top" alt="{{ $game['titolo'] }}">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $game['titolo'] }}</h5>
                                    <p class="card-text">Piattaforma: {{ $game['piattaforma'] }}</p>
                                    <p class="card-text">Uscita: {{ $game['uscita'] }}</p>
                                    <a href="{{ route('dettagli', ['id' => $game['id']]) }}" class="btn btn-primary">Dettagli</a>
                                </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 my-3">
                <h2>Non esistono risultati per la tua ricerca</h2>
            </div>
            @endif
        </div>
    </div>

</x-layout>