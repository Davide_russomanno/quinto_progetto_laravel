<x-layout>
<div class="sfondo d-flex align-items-center">
    <div class="container my-5">
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="{{ $game['img-2'] }}" alt="{{ $game['titolo'] }}">
            </div>
            <div class="col-12 col-md-6">
                <h1>{{ $game['titolo'] }}</h1>
                <h4 class="my-4">Piattaforma: {{ $game['piattaforma'] }}</h4>
                <h4>Uscita: {{ $game['uscita'] }}</h4>
                <h4 class="my-4">Prezzo: {{ $game['prezzo'] }}</h4>
            </div>
        </div>
    </div>
    </div>

</x-layout>

<!-- [
            'id' => '1',
            'img' => '/media/OnePiece.jpg',
            'titolo' => 'One Piece Odyssey',
            'piattaforma' => 'PS5, PS4, Xbox Series X|S, Xbox One, PC',
            'uscita' => 'Gennaio 13'
        ], -->