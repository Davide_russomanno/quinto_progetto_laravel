<x-layout>

    <div class="sfondo d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1 class="font-h1">Sei un appassionato del mondo del game?</h1>
                    <h3 class="font-h3 my-3">Allora sei nel posto giusto!</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid sfondo-titoli">
        <div class="row">
            <div class="col-12 my-5">
                <h2 class="d-flex justify-content-center">Nuove uscite 2023</h2>
            </div>
        </div>
        <div class="row">
            @foreach($games as $game)
                <div class="col-12 col-md-3 my-5">
                    <div class="card" style="width: 18rem;">
                        <img src="{{ $game['img'] }}" class="card-img-top" alt="{{ $game['titolo'] }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $game['titolo'] }}</h5>
                                <p class="card-text">Piattaforma: {{ $game['piattaforma'] }}</p>
                                <p class="card-text">Uscita: {{ $game['uscita'] }}</p>
                                <a href="{{ route('dettagli', ['id' => $game['id']]) }}" class="btn btn-primary">Dettagli</a>
                            </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</x-layout>