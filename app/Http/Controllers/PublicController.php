<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{

    public $games = [
        [
            'id' => '1',
            'img' => '/media/OnePiece.jpg',
            'img-2' => '/media/OnePiece.jpg',
            'titolo' => 'One Piece Odyssey',
            'piattaforma' => 'PS5, PS4, Xbox Series X|S, Xbox One, PC',
            'uscita' => '2023',
            'prezzo' => '€70,00'
        ],
        [
            'id' => '2',
            'img' => '/media/space.jpg',
            'img-2' => '/media/space.jpg',
            'titolo' => 'A Space for the Unbound',
            'piattaforma' => 'PS5, PS4, Xbox Series X|S, Xbox One, PC',
            'uscita' => '2023',
            'prezzo' => '€30,00'
        ],
        [
            'id' => '3',
            'img' => '/media/Colossal.avif',
            'img-2' => '/media/Colossal.avif',
            'titolo' => 'Colossal Cave',
            'piattaforma' => 'PS5, Xbox Series X|S, Switch, Quest, PC',
            'uscita' => '2023',
            'prezzo' => '€35,00'
        ],
        [
            'id' => '4',
            'img' => '/media/Forspoken.webp',
            'img-2' => '/media/forspoken-2.jpg',
            'titolo' => 'Forspoken',
            'piattaforma' => 'PS5, PC',
            'uscita' => '2023',
            'prezzo' => '€80,00'
        ],
        [
            'id' => '4',
            'img' => '/media/Forspoken.webp',
            'img-2' => '/media/forspoken-2.jpg',
            'titolo' => 'Forspoken',
            'piattaforma' => 'PS5, PC',
            'uscita' => '2022',
            'prezzo' => '€80,00'
        ],
        [
            'id' => '4',
            'img' => '/media/Forspoken.webp',
            'img-2' => '/media/forspoken-2.jpg',
            'titolo' => 'Forspoken',
            'piattaforma' => 'PS5, PC',
            'uscita' => '2022',
            'prezzo' => '€80,00'
        ],
        [
            'id' => '4',
            'img' => '/media/Forspoken.webp',
            'img-2' => '/media/forspoken-2.jpg',
            'titolo' => 'Forspoken',
            'piattaforma' => 'PS5, PC',
            'uscita' => '2022',
            'prezzo' => '€80,00'
        ],

    ];


    public function home() {
        return view('welcome', ['games' => $this->games]);
    }

    public function dettagli($id){
        foreach($this->games as $game){
            if($game['id'] == $id){
                return view('dettagli', ['game' => $game]);
            }
        }
    }

    public function ricerca(Request $request){
        $key = $request->query('chiaveRicerca');

        $giocoFiltrato = [];

        foreach($this->games as $game){
            if(str_contains(strtolower($game['titolo']), strtolower($key))){
                $giocoFiltrato[] = $game;
            }
        }
        return view('ricerca', ['games' => $giocoFiltrato, 'key' => $key]);
    }
}


