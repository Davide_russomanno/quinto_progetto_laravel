<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;


Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/dettagli/{id}', [PublicController::class, 'dettagli'])->name('dettagli');
Route::get('/ricerca', [PublicController::class, 'ricerca'])->name('ricerca');
